use std::{mem::transmute, fs::File, io::{BufReader, Read}, env,};

use pixels::{SurfaceTexture, Pixels};
use winit::{event_loop::{EventLoop, ControlFlow}, dpi::LogicalSize, event::Event, window::WindowBuilder};

mod cpu;

const DISPLAY_HEIGHT: usize = 32;
const DISPLAY_WIDTH: usize = 64;
const DISPLAY_SCALE: usize = 8;

struct Display {
    
}

impl Display {
    fn new() -> Self {
        Self {}
    }
    
    fn update(&mut self) {
        
    }
    
    fn draw(&self, frame: &mut [u8], video_memory: &[[u8; DISPLAY_WIDTH]; DISPLAY_HEIGHT]) {
        let video_memory: [u8; DISPLAY_WIDTH * DISPLAY_HEIGHT] = unsafe { transmute(*video_memory) };
        
        for i in 0..video_memory.len() {
            frame[i*4+0] = video_memory[i] * 255;
            frame[i*4+1] = video_memory[i] * 255;
            frame[i*4+2] = video_memory[i] * 255;
            frame[i*4+3] = 0xff;
        }
        /*for (i, pixel) in frame.chunks_exact_mut(4).enumerate() {
            let _x = (i % DISPLAY_WIDTH) as usize;
            let _y = (i / DISPLAY_WIDTH) as usize;
            let a =video_memory[1][1];
            if _x == x && _y == y {
                pixel[0] = 255;
                pixel[1] = 255;
                pixel[2] = 255;
                pixel[3] = 255;
            }
            
            //let rgba = if x == 60 || y == 22 {[255,255,255,255]} else {[0,0,0,0]}; 
            //pixel.copy_from_slice(&rgba);
        }*/
    }
}

fn main() {
    let event_loop = EventLoop::new();
    let window = {
        let size = LogicalSize::new( (DISPLAY_WIDTH * DISPLAY_SCALE) as u32, (DISPLAY_HEIGHT * DISPLAY_SCALE) as u32 );
        WindowBuilder::new()
            .with_title("title")
            .with_inner_size(size)
            .with_min_inner_size(size)
            .build(&event_loop).unwrap()
        };
        
    let mut matrix = {
        let window_size = window.inner_size();
        let surface_texture = SurfaceTexture::new(window_size.width, window_size.height, &window);
        Pixels::new(DISPLAY_WIDTH as u32, DISPLAY_HEIGHT as u32, surface_texture).unwrap()
        };
        
    let display = Display::new();
    
    let mut cpu = cpu::CPU::new();
    
    let args: Vec<String> = env::args().collect();
    let file_path = &args[1];
    
    let game = File::open(file_path);
    let game = game.unwrap();
    let mut game = BufReader::new(game);
    let mut buf = [0u8; 4096 - 512];
    game.read(&mut buf);
    for i in 0..buf.len() {
        cpu.memory[512 + i] = buf[i];
    }
    
    cpu.position_in_memory = 512;
    event_loop.run(move |event, _, control_flow| {
        if let Event::RedrawRequested(_) = event {
            
            let output = cpu.tick();
            display.draw(matrix.frame_mut(), &cpu.video_memory);
            if let Err(_) = matrix.render() {
                *control_flow = ControlFlow::Exit;
                return;
            }
        }
        //display.update();
        window.request_redraw();
    });
    
} 
