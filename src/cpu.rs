use rand::prelude::*;

const DISPLAY_HEIGHT: usize = 32;
const DISPLAY_WIDTH: usize = 64;
const DISPLAY_SCALE: usize = 8;


pub struct OutputState {
    video_memory: [[u8; DISPLAY_WIDTH]; DISPLAY_HEIGHT]
}

enum ProgramCounter {
    Next,
    Skip,
    Jump(usize),
}

impl ProgramCounter {
    fn skip_if(condition: bool) -> ProgramCounter {
        if condition {
            ProgramCounter::Skip
        } else {
            ProgramCounter::Next
        }
    }
}

pub struct CPU {
    pub registers: [u8; 16],
    pub register_i: u16,    // This register is generally used to store memory addresses
    pub register_dt: u8,
    pub register_st: u8,
    pub position_in_memory: usize,
    pub memory: [u8; 0x1000],
    pub stack: [u16; 16],
    pub stack_pointer: usize,
    pub video_memory: [[u8; DISPLAY_WIDTH]; DISPLAY_HEIGHT],
    pub video_memory_change: bool,
    pub keypad: [bool; 16],
    pub keypad_wait: bool,
    pub keypad_reg: u8,
}

impl CPU {
    pub fn new() -> CPU {
        CPU {
            registers: [0; 16],
            register_i: 0,
            register_dt: 0,
            register_st: 0,
            position_in_memory: 0,
            memory: [0; 0x1000],
            stack: [0; 16],
            stack_pointer: 0,
            video_memory: [[0; DISPLAY_WIDTH]; DISPLAY_HEIGHT],
            video_memory_change: false,
            keypad: [false; 16],
            keypad_wait: false,
            keypad_reg: 0,
        }
    }
    
    fn read_opcode(&self) -> u16 {
        let p = self.position_in_memory;
        let op_byte1 = self.memory[p] as u16;
        let op_byte2 = self.memory[p + 1] as u16;
        
        op_byte1 << 8 | op_byte2
    }
    
    pub fn tick(&mut self) -> OutputState {
        if self.keypad_wait {
            for i in 0..self.keypad.len() {
                if self.keypad[i] {
                    self.keypad_wait = false;
                    self.registers[self.keypad_reg as usize] = i as u8;
                    break;
                }
            }
        } else {
            if self.register_dt > 0 {self.register_dt -= 1;}
            if self.register_st > 0 {self.register_st -= 1;}
            let opcode = self.read_opcode();
            self.run_opcode(opcode);
        }

        OutputState{
            video_memory: self.video_memory,
        }

    }
    
    fn run_opcode(&mut self, opcode: u16) {
        println!("tick: pos: {}, opcode: {:01x}", self.position_in_memory,self.read_opcode());         
        let nibbles = (
            ((opcode & 0xF000) >> 12) as u8,
            ((opcode & 0x0F00) >>  8) as u8,
            ((opcode & 0x00F0) >>  4) as u8,
            ((opcode & 0x000F) >>  0) as u8,
        );
        let nnn = opcode & 0x0FFF;
        let kk = (opcode & 0x00FF) as u8;
        let x = nibbles.1;
        let y = nibbles.2;
        let n = nibbles.3;
            
        //dbg!("tick : {:?}       opcode: {:x}", self.position_in_memory, opcode);
            
        let pc = match nibbles {
            (0x0, 0x0, 0x0, 0x0) => { ProgramCounter::Next },
            //(  0,   _,   _,   _) => {  },   // ignore
            (  0,   0, 0xE,   0) => self.cls(),
            (  0,   0, 0xE, 0xE) => self.ret(),
            (  1,   _,   _,   _) => self.jump(nnn),
            (  2,   _,   _,   _) => self.call(nnn),
            (  3,   _,   _,   _) => self.skip_equal_xkk(x, kk),
            (  4,   _,   _,   _) => self.skip_not_equal(x, kk),
            (  5,   _,   _,   0) => self.skip_equal_xy(x, y),
            (  6,   _,   _,   _) => self.set_value(x, kk),
            (  7,   _,   _,   _) => self.add_value(x, kk),
            (  8,   _,   _,   0) => self.set_value_from_register(x, y),
            (  8,   _,   _,   1) => self.or(x, y),
            (  8,   _,   _,   2) => self.and(x, y),
            (  8,   _,   _,   3) => self.xor(x, y),
            (  8,   _,   _, 0x4) => self.add_xy(x, y),
            (  8,   _,   _,   5) => self.sub_xy(x, y),
            (  8,   _,   _,   6) => self.div_by_2(x),
            (  8,   _,   _,   7) => self.sub_yx(x, y),
            (  8,   _,   _, 0xE) => self.mul_by_2(x),
            (  9,   _,   _,   0) => self.skip_not_equal_xy(x, y),
            (0xA,   _,   _,   _) => self.set_value_for_i(nnn),
            (0xB,   _,   _,   _) => self.jump_plus(nnn),
            (0xC,   _,   _,   _) => self.random(x, kk),
            (0xD,   _,   _,   _) => self.draw(x, y, n),
            (0xE,   _,   9, 0xE) => self.skip_if_press(x),
            (0xE,   _, 0xA,   1) => self.skip_if_not_press(x),
            (0xF,   _,   0,   7) => self.get_dt_value(x),
            (0xF,   _,   0, 0xA) => self.wait_for_key_press(x),
            (0xF,   _,   1,   5) => self.set_dt_value(x),
            (0xF,   _,   1,   8) => self.set_st_value(x),
            (0xF,   _,   1, 0xE) => self.increase_reg_i(x),
            (0xF,   _,   2,   9) => self.set_reg_i_sprite(x),
            (0xF,   _,   3,   3) => self.store_bcd(x),
            (0xF,   _,   5,   5) => self.save_registers_in_mem(x),
            (0xF,   _,   6,   5) => self.read_registers_from_mem(x),
            _ => todo!("opcode {:04x}", opcode)
        };
        
        match pc {
            ProgramCounter::Next => self.position_in_memory += 2,
            ProgramCounter::Skip => self.position_in_memory += 4,
            ProgramCounter::Jump(addr) => self.position_in_memory = addr,
        }
    }
    
    /// opcode: 0nnn
    fn sys(&mut self) {
        /*This instruction is only used on the old computers on which Chip-8 was
        originally implemented. It is ignored by modern interpreters.*/
    }
    
    /// opcode: 00E0
    fn cls(&mut self) -> ProgramCounter {
        for y in 0..DISPLAY_HEIGHT {
            for x in 0..DISPLAY_WIDTH {
                self.video_memory[y][x] = 0;
            }
        }
        self.video_memory_change = true;
        ProgramCounter::Next
    }
    
    /// opcode: 00EE
    fn ret(&mut self) -> ProgramCounter {
        if self.stack_pointer == 0 {
            panic!("Stack underflow");
        }
        
        self.stack_pointer -= 1;
        let call_addr = self.stack[self.stack_pointer];
        ProgramCounter::Jump(call_addr as usize)
    }
    
    /// opcode: 1nnn
    fn jump(&mut self, addr: u16) -> ProgramCounter {
        ProgramCounter::Jump(addr as usize)
    }
    
    /// opcode: 2nnn
    fn call(&mut self, addr: u16) -> ProgramCounter {
        let sp = self.stack_pointer;
        let stack = &mut self.stack;
        
        if sp > stack.len() {
            panic!("Stack overflow!");
        }
        
        stack[sp] = self.position_in_memory as u16 + 2;
        self.stack_pointer += 1;
        ProgramCounter::Jump(addr as usize)
    }
    
    /// opcode: 3xkk
    fn skip_equal_xkk(&mut self, x:u8, kk:u8) -> ProgramCounter {
        ProgramCounter::skip_if(self.registers[x as usize] == kk)
    }
    
    /// opcode: 4xkk
    fn skip_not_equal(&mut self, x:u8, kk:u8) -> ProgramCounter {
        ProgramCounter::skip_if(self.registers[x as usize] != kk)
    }
    
    /// opcode: 5xy0
    fn skip_equal_xy(&mut self, x:u8, y:u8) -> ProgramCounter {
        ProgramCounter::skip_if(self.registers[x as usize] == self.registers[y as usize])
    }
    
    /// opcode: 6xkk
    fn set_value(&mut self, x:u8, kk:u8) -> ProgramCounter {
        self.registers[x as usize] = kk;
        ProgramCounter::Next
    }
    
    /// opcode: 7xkk
    fn add_value(&mut self, x:u8, kk:u8) -> ProgramCounter {
        self.registers[x as usize] += kk;
        ProgramCounter::Next
    }
    
    /// opcode: 8xy0
    fn set_value_from_register(&mut self, x:u8, y:u8) -> ProgramCounter {
        self.registers[x as usize] = self.registers[y as usize];
        ProgramCounter::Next
    }
    
    /// opcode: 8xy1
    fn or(&mut self, x:u8, y:u8) -> ProgramCounter {
        self.registers[x as usize] =
            self.registers[x as usize] | self.registers[y as usize];
        ProgramCounter::Next
    }
    
    /// opcode: 8xy2
    fn and(&mut self, x:u8, y:u8) -> ProgramCounter {
        self.registers[x as usize] =
            self.registers[x as usize] & self.registers[y as usize];
        ProgramCounter::Next
    }
    
    /// opcode: 8xy3
    fn xor(&mut self, x:u8, y:u8) -> ProgramCounter {
        self.registers[x as usize] =
            self.registers[x as usize] ^ self.registers[y as usize];
        ProgramCounter::Next
    }
    
    /// opcode: 8xy4
    fn add_xy(&mut self, x:u8, y:u8) -> ProgramCounter {
        let arg1 = self.registers[x as usize];
        let arg2 = self.registers[y as usize];
        
        let (val, overflow) = arg1.overflowing_add(arg2);
        self.registers[x as usize] = val;
        
        if overflow {
            self.registers[0xF] = 1;
        } else {
            self.registers[0xF] = 0;
        }
        ProgramCounter::Next
    }

    /// opcode: 8xy5
    fn sub_xy(&mut self, x:u8, y:u8) -> ProgramCounter {
        let arg1 = self.registers[x as usize];
        let arg2 = self.registers[y as usize];
        
        if arg1 > arg2{
            self.registers[0xF] = 1;
        } else {
            self.registers[0xF] = 0;
        }
        self.registers[x as usize] = arg1 - arg2;
        ProgramCounter::Next
    }
    
    /// opcode: 8x06
    fn div_by_2(&mut self, x:u8) -> ProgramCounter {
        let arg1 = self.registers[x as usize];
        
        if arg1 & 0b00000001 == 1 {
            self.registers[0xF] = 1;
        } else {
            self.registers[0xF] = 0;
        }
        self.registers[x as usize] >>= 1;
        ProgramCounter::Next
    }
    
    /// opcode: 8xy7
    fn sub_yx(&mut self, x:u8, y:u8) -> ProgramCounter {
        let arg1 = self.registers[x as usize];
        let arg2 = self.registers[y as usize];
        
        if arg2 > arg1{
            self.registers[0xF] = 1;
        } else {
            self.registers[0xF] = 0;
        }
        self.registers[x as usize] = arg2 - arg1;
        ProgramCounter::Next
    }
    
    /// opcode: 8x0E
    fn mul_by_2(&mut self, x:u8) -> ProgramCounter {
        let arg1 = self.registers[x as usize];
        
        if arg1 & 0b10000000 == 1 {
            self.registers[0xF] = 1;
        } else {
            self.registers[0xF] = 0;
        }
        self.registers[x as usize] <<= 1;
        ProgramCounter::Next
    }
    
    /// opcode: 9xy0
    fn skip_not_equal_xy(&mut self, x:u8, y:u8) -> ProgramCounter {
        ProgramCounter::skip_if(self.registers[x as usize] != self.registers[y as usize])
    }
    
    /// opcode: Annn
    fn set_value_for_i(&mut self, addr: u16) -> ProgramCounter {
        self.register_i = addr;
        ProgramCounter::Next
    }
    
    /// opcode: Bnnn
    fn jump_plus(&mut self, addr: u16) -> ProgramCounter {
        let val = self.registers[0] as usize;
        ProgramCounter::Jump(addr as usize + val)
    }
    
    /// opcode: Cxkk
    fn random(&mut self, x:u8, kk:u8) -> ProgramCounter {
        let mut r: u8 = thread_rng().gen();
        r &= kk;
        self.registers[x as usize] = r;
        ProgramCounter::Next
    }
    
    /// opcode: Dxyn
    fn draw(&mut self, x:u8, y:u8, n:u8) -> ProgramCounter {
        self.registers[0xf] = 0;
        
        for byte in 0..n {
            let y = (self.registers[y as usize] + byte) as usize % DISPLAY_HEIGHT;
            for bit in 0..8 {
                let x = (self.registers[x as usize] + bit) as usize % DISPLAY_WIDTH;
                let color = self.memory[self.register_i as usize + byte as usize] >> (7-bit) & 1;
                self.registers[0xf] |= color & self.video_memory[y][x];
                self.video_memory[y][x] ^= color;
            }
        }
        ProgramCounter::Next
    }
    
    /// opcode: Ex9E
    fn skip_if_press(&mut self, x:u8) -> ProgramCounter {
        ProgramCounter::skip_if(self.keypad[self.registers[x as usize] as usize])
    }
    
    /// opcode: ExA1
    fn skip_if_not_press(&mut self, x:u8) -> ProgramCounter {
        ProgramCounter::skip_if(self.keypad[self.registers[x as usize] as usize] != true)
    }
   
    /// opcode: Fx07
    fn get_dt_value(&mut self, x:u8) -> ProgramCounter {
        self.registers[x as usize] = self.register_dt;
        ProgramCounter::Next
    }
    
    /// opcode: Fx0A
    fn wait_for_key_press(&mut self, x:u8) -> ProgramCounter {
        self.keypad_reg = x;
        self.keypad_wait = true;
        ProgramCounter::Next
    }
    
    /// opcode: Fx15
    fn set_dt_value(&mut self, x:u8) -> ProgramCounter {
        self.register_dt = self.registers[x as usize];
        ProgramCounter::Next
    }
    
    /// opcode: Fx18
    fn set_st_value(&mut self, x:u8) -> ProgramCounter {
        self.register_st = self.registers[x as usize];
        ProgramCounter::Next
    }
    
    /// opcode: Fx1E
    fn increase_reg_i(&mut self, x:u8) -> ProgramCounter {
        self.register_i += self.registers[x as usize] as u16;
        ProgramCounter::Next
    }
    
    /// opcode: Fx29
    fn set_reg_i_sprite(&mut self, x:u8) -> ProgramCounter {
        self.register_i = ( self.registers[x as usize] * 5 ) as u16;
        ProgramCounter::Next
    }
    
    /// opcode: Fx33
    fn store_bcd(&mut self, x:u8) -> ProgramCounter {
        self.memory[self.register_i as usize] = self.registers[x as usize] / 100;
        self.memory[self.register_i as usize + 1] = (self.registers[x as usize] % 100) / 10;
        self.memory[self.register_i as usize + 2] = self.registers[x as usize] % 10;
        ProgramCounter::Next
    }
    
    /// opcode: Fx55
    fn save_registers_in_mem(&mut self, x:u8) -> ProgramCounter {
        for i in 0..x+1 {
            self.memory[self.register_i as usize + i as usize] = self.registers[i as usize]; 
        }
        ProgramCounter::Next
    }
    
    /// opcode: Fx65
    fn read_registers_from_mem(&mut self, x:u8) -> ProgramCounter {
        for i in 0..x+1 {
            self.registers[i as usize] = self.memory[self.register_i as usize + i as usize]; 
        }
        ProgramCounter::Next
    } 
    
}